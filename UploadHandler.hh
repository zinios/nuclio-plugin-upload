<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\upload
{
	interface UploadHandler
	{	
		public function put(string $file, string $destination):bool;

		public function move(string $target, string $destination):bool;
		
		public function copy(string $target, string $destination):bool;

		public function delete(string $target):bool;
	}
}
