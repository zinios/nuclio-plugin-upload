<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\upload
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\http\request\Request;
	use nuclio\plugin\upload\UploadException;

	use nuclio\plugin\upload\UploadHandler;
	use nuclio\plugin\upload\S3UploadHandler as S3;
	use nuclio\plugin\upload\LocalUploadHandler as Local;

	class Upload extends Plugin
	{
		private Request 		$request;
		private UploadHandler 	$handler;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Upload
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(string $handler, ?string $awsHost=null, ?string $awsKey=null, ?string $awsSecret=null)
		{
			parent::__construct();
			$this->request = Request::getInstance();
			if($handler == 'S3')
			{
				if(is_null($awsHost) || is_null($awsKey) || is_null($awsSecret))
				{
					throw new UploadException("Missing parameter");
				}
				$this->handler = S3::getInstance($awsHost, $awsKey, $awsSecret);
			}
			else if($handler == 'local')
			{
				$this->handler = Local::getInstance();
			}
			else
			{
				throw new UploadException("Unsupported handler.");
			}
		}

		public function upload(string $name, string $destination):bool 
		{
			$file = $this->request->getFile($name);
			if(is_null($file))
			{
				throw new UploadException("No file");
			}
			return $this->handler->put($file['tmp_name'], $destination);
		}

		public function copy(string $target, string $destination):bool
		{
			return  $this->handler->copy($target, $destination);
		}

		public function move(string $target, string $destination):bool
		{
			return  $this->handler->move($target, $destination);
		}

		public function delete(string $target):bool
		{
			return $this->handler->delete($target);
		}

		public function put(string $name, string $destination):bool
		{
			return $this->handler->put($name, $destination);
		}
	}
}
