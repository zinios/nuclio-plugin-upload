<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\upload
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;

	use nuclio\plugin\upload\UploadHandler;

	class LocalUploadHandler extends Plugin implements UploadHandler
	{

		public static function getInstance(/* HH_FIXME[4033] */...$args):LocalUploadHandler
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self();
		}

		public function put(string $file, string $destination):bool
		{
			return move_uploaded_file($file, $destination);
		}

		public function move(string $target, string $destination):bool
		{
			return rename($target, $destination);
		}

		public function delete(string $target):bool
		{
			return unlink($target);
		}
	}
}
