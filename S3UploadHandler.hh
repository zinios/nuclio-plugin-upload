<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\upload
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;

	use nuclio\plugin\upload\UploadHandler;
	use Aws\S3\S3Client;
	use Aws\S3\Exception;

	class S3UploadHandler extends Plugin implements UploadHandler
	{
		const S3URL = 's3.amazonaws.com';
		private S3Client $client;

		public static function getInstance(/* HH_FIXME[4033] */...$args):S3UploadHandler
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(string $host, string $awsKey, string $awsSecretKey)
		{
			parent::__construct();
			$this->client = S3Client::factory
			(
				[
					//'base_url' => $host,
					'key'		=>$awsKey,
					'secret'	=>$awsSecretKey,
					'http'		=>
					[
						'timeout'	=>1800
					]
				]
			);
		}

		public function put(string $file, string $destination):bool
		{
			//list($key,$bucket) = $this->pathToKeyAndBucket($destination);
			$parts = explode('/', $destination);
			$bucket = $parts[0];
			array_shift($parts);
			$key = implode('/', $parts);
			if($key=='')
			{
				return false;
			}
			$result = $this->client->putObject
			(
				[
				    'Bucket' => $bucket,
				    'Key'    => $key,
				    'Body'   => fopen($file, 'r')
				]
			);
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function copy(string $target, string $destination):bool
		{
			$parts = explode('/', $target);
			$sourceBucket = $parts[0];
			array_shift($parts);
			$sourceKey = implode('/', $parts);
			if($sourceKey=='')
			{
				return false;
			}

			$parts = explode('/', $destination);
			$bucket = $parts[0];
			array_shift($parts);
			$key = implode('/', $parts);
			if($key=='')
			{
				return false;
			}
			$this->client->copyObject
			(
				[
					'ACL' => 'public-read',
					'CopySource' => "{$sourceBucket}/{$sourceKey}",
					'Bucket' => $bucket,
					'Key' => $key
				]
			);
			return true;
		}

		public function move(string $target, string $destination):bool
		{
			$parts = explode('/', $target);
			$sourceBucket = $parts[0];
			array_shift($parts);
			$sourceKey = implode('/', $parts);
			if($sourceKey=='')
			{
				return false;
			}

			$parts = explode('/', $destination);
			$bucket = $parts[0];
			array_shift($parts);
			$key = implode('/', $parts);
			if($key=='')
			{
				return false;
			}
			$this->client->copyObject
			(
				[
					'ACL' => 'public-read',
					'CopySource' => "{$sourceBucket}/{$sourceKey}",
					'Bucket' => $bucket,
					'Key' => $key
				]
			);
			// $this->client->deleteObject
			// (
			// 	[
			// 		'Bucket' => $sourceBucket,
			// 		'Key' => $sourceKey
			// 	]
			// );
			return true;
		}

		public function delete(string $target):bool
		{
			$parts = explode('/', $target);
			$bucket = $parts[0];
			array_shift($parts);
			$key = implode('/', $parts);
			if($key=='')
			{
				return false;
			}
			$result = $this->client->deleteObject
			(
				[
				    'Bucket' => $bucket,
				    'Key' => $key
				]
			);
			if($result)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
